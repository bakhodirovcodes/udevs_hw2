import "./styles.module.scss"

const CTable = ({ children }) => <table>{children}</table>

CTable.Thead = ({ children }) => <thead>{children}</thead>

CTable.Tbody = ({ children }) => <tbody>{children}</tbody>

CTable.Tr = ({ children }) => <tr>{children}</tr>

CTable.Th = ({ children }) => <th>{children}</th>

CTable.Td = ({ children }) => <td>{children}</td>

export default CTable
