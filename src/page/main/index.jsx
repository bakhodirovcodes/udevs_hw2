import CTable from "../../components/CElements/CTable"
import useTable from "../../hooks"
import CPagination from "../../components/CElements/CPagination"

import { column, row } from "./data"

const MainPage = () => {
  const {
    currentRows,
    currentPage,
    totalPages,
    nextPage,
    previousPage,
    setPage,
  } = useTable(row, 10)

  return (
    <div className="table">
      <CTable>
        <CTable.Thead>
          <CTable.Tr>
            {column?.map((item) => (
              <CTable.Th key={item?.id}>{item?.name}</CTable.Th>
            ))}
          </CTable.Tr>
        </CTable.Thead>

        <CTable.Tbody>
          {currentRows?.map((item) => (
            <CTable.Tr key={item.id}>
              <CTable.Td>{item.id}</CTable.Td>
              <CTable.Td>{item.name}</CTable.Td>
              <CTable.Td>{item.email}</CTable.Td>
            </CTable.Tr>
          ))}
        </CTable.Tbody>
      </CTable>
      <CPagination>
        <CPagination.Left
          disabled={currentPage === 1}
          handleClick={previousPage}
        />
        <CPagination.Pages
          currentPage={currentPage}
          setPage={setPage}
          totalPages={totalPages}
          count={Math.ceil(row.length / 10)}
          current={currentPage}
        />
        <CPagination.Right
          disabled={currentPage === Math.ceil(row.length / 10)}
          handleClick={nextPage}
        />
      </CPagination>
    </div>
  )
}

export default MainPage
