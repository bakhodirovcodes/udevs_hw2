export const row = []

const initialValue = {
  id: 1,
  name: "id labore ex et quam laborum",
  email: "Eliseo@gardner.biz",
}

for (let i = 1; i <= 200; i++) {
  const newRow = {
    ...initialValue,
    id: i,
  }
  row.push(newRow)
}

export const column = [
  {
    id: 1,
    name: "№",
  },
  {
    id: 2,
    name: "Name",
  },
  {
    id: 3,
    name: "Email",
  },
]
